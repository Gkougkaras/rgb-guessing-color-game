var numSquares = (12);
var colors = generateRandomColors(numSquares);
var squares = document.querySelectorAll(".square");
var selectedColor = document.getElementById("selectedColor");
var messageDisplay = document.querySelector("#message");
var selection =pickColor();
var h1 = document.querySelector("h1");
var resetButton = document.querySelector("#reset");
var modeButtons = document.querySelectorAll(".mode");

init();

function init(){
setUpModeButtons();
setUpSquares();  
reset();
}

//!---------------Mode buttons--------------------!
function setUpModeButtons(){
    for (var i=0; i < modeButtons.length; i++){
        modeButtons[i].addEventListener("click", function(){
          modeButtons[0].classList.remove("selected");
          modeButtons[1].classList.remove("selected");
            modeButtons[2].classList.remove("selected");
            this.classList.add("selected");
            this.textContent === "Easy" ? numSquares =6: this.textContent ==="Normal" ? numSquares=9:  numSquares =12;
            reset();
         })
    };
}

//!---------------Square Functionality--------------------!
function setUpSquares(){
    for (var i=0; i < squares.length; i++){
        squares[i].addEventListener("click", function(){
            var clickedColor = this.style.backgroundColor;
            if (clickedColor === selection){
                messageDisplay.textContent="Correct!!";
                resetButton.textContent= "Play Again?";
                changeColors(clickedColor);
                h1.style.backgroundColor = clickedColor;
            }
            else{
                this.style.backgroundColor="#232323";
                messageDisplay.textContent="Try Again"
            }
        });
    }
}

//!---------------Reset mode--------------------!
function reset() {
    colors = generateRandomColors(numSquares);
    selection = pickColor();
    selectedColor.textContent = selection;
    messageDisplay.textContent = " ";
    resetButton.textContent ="New Colors";
    for (var i=0; i < squares.length; i++){
        if (colors[i]){
            squares[i].style.display = "block";
            squares[i].style.backgroundColor = colors[i];
        }
        else{
             squares[i].style.display ="none";
         }
    }
    h1.style.backgroundColor = "rgb(39, 192, 202)";
}

resetButton.addEventListener("click",function(){
    reset();
}
);

function changeColors(color){
    for (var i=0; i < squares.length; i++){
        squares[i].style.backgroundColor=color;
    }
}

function pickColor(color){
    var random = Math.floor(Math.random() * colors.length);
    return colors[random];
}

function generateRandomColors(num){
    var arr=[];
    for (var i= 0; i< num; i++){
        arr.push(randomColor());
    }
    return arr;
}

function randomColor(){
    var r = Math.floor(Math.random() * 256);
    var g = Math.floor(Math.random() * 256);
    var b = Math.floor(Math.random() * 256);
    return "rgb(" + r  + "," + " " + g + "," + " "  + b + ")";
}