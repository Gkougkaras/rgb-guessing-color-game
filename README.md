This is a project i created during taking JavaScript Courses.

//!---How it works--

It is a game of finding the correct color according to its coresponding RGB value,
that is displayed on the screen.

Every time we reset, choose to get new colors or change the difficulty,
the game randomly chooses from an array of auto-generated numbers the values for 
red, green and blue and it passes them in an order to the squares with a selector.

If the player makes a wrong choice the game notifies them that they should try again.
and makes the block invinsible.

When the right choice is made all the squares along with the header of the game are
inheriting the correct answer's color and a message correct is displayed.

The player can choose between 12 blocks(initial hard difficulty) 9 blocks(normal)
and 6 blocks easy).
